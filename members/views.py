from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from reportlab.pdfgen import canvas

from reportlab.lib import colors
import reportlab.lib.pagesizes as pagesizes
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.platypus import Paragraph, Spacer
from reportlab.lib.styles import getSampleStyleSheet

import reportlab.rl_config
reportlab.rl_config.warnOnMissingFontGlyphs = 0

# The following configures extra reportlab fonts.
import os
from django.conf import settings
reportlab.rl_config.TTFSearchPath.append(os.path.join(settings.BASE_DIR, 'reportlab_extra_fonts'))
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
HEBREW_FONT_NAME = 'David'
pdfmetrics.registerFont(TTFont(HEBREW_FONT_NAME, 'DavidCLM-Medium.ttf'))
# The above configures extra reportlab fonts.

from bidi.algorithm import get_display

import csv
import datetime
import io
import re
import sys

from .models import Person, ContactType, Contact, Comment, Status

########################################################################
# Custom Views
########################################################################

PGSQL_RESET_SEQUENCE_SQL = """BEGIN;
SELECT setval(pg_get_serial_sequence('"members_person"','id_member'), coalesce(max("id_member"), 1), max("id_member") IS NOT null) FROM "members_person";
SELECT setval(pg_get_serial_sequence('"members_contacttype"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "members_contacttype";
SELECT setval(pg_get_serial_sequence('"members_contact"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "members_contact";
SELECT setval(pg_get_serial_sequence('"members_comment"','id'), coalesce(max("id"), 1), max("id") IS NOT null) FROM "members_comment";
COMMIT;"""

def _is_row_empty(row):
    """Check if the csv.DictReader-created row has only empty fields."""
    for key in row:
        if ((row[key] is not None) and (len(row[key].strip()) > 0)):
            return False
    return True
    
@login_required
def importCSV(request):
    if request.method == 'POST':
        #totallen = 0
        #for chunk in request.FILES['csvfile']:
        #    totallen += len(chunk)
        #    sys.stdout.write(chunk.decode())
        if ('csvfile' not in request.FILES):
            return HttpResponseRedirect(reverse('main', kwargs={'statusmsg' : "CSV import was requested without specifying CSV filename."}))
        csvreader = csv.DictReader(io.StringIO(request.FILES['csvfile'].read().decode()))
        counts = {'person' : 0, 'contact' : 0, 'comment' : 0}
        statusmessages = []
        for row in csvreader:
            if _is_row_empty(row):
                statusmessages.append("An empty row was encountered and skipped\n")
                continue
            if not ((row['email_address'] != None) and (len(row['email_address'].strip()) > 0)):
                if (row['status'] == 'Member'):
                    # Members must have E-mail address.
                    statusmessages.append("Empty E-mail address for %(member_no)s:%(status)s   %(hname)s %(hsir_name)s (%(name)s %(sir_name)s)\n" % row)
                    continue
            #sys.stdout.write("%(member_no)s: %(status)s   %(hname)s %(hsir_name)s    gender:'%(male)s'\n" % row)

            try:
                mo = re.search(r'^\s*(\d+)-(\d+)-(\d+)\s*$', row['validity'])
                membership_end = datetime.date(int(mo.group(1)), int(mo.group(2)), int(mo.group(3)))
            except AttributeError:
                membership_end = None
            try:
                mo = re.search(r'^\s*(\d+)/(\d+)/(\d+)\s*$', row['birthdate'])
                birthday = datetime.date(int(mo.group(3)), int(mo.group(2)), int(mo.group(1)))
            except AttributeError:
                birthday = None

            if (row['male'].strip() == 't'):
                gender = Person.GENDER_MALE
            elif (row['male'].strip() == 'f'):
                gender = Person.GENDER_FEMALE
            else:
                gender = None  # Unknown

            person = Person(id_member=row['member_no'],
                                id_official=row['id_no'],
                                id_type=Person.IDTYPE_TEUDATZEHUT,
                                email=row['email_address'],
                                name_surname_heb=row['hsir_name'],
                                name_firstname_heb=row['hname'],
                                name_surname_eng=row['sir_name'],
                                name_firstname_eng=row['name'],
                                status=(Person.STATUS_ACTIVEMEMBER if row['status'] == 'Member' else Person.STATUS_FRIEND),
                                membership_end=membership_end,
                                membership_start=None,
                                address_street_number=row['street_address'],
                                address_city=row['city'],
                                address_zipcode=row['zipcode'],
                                address_country='IL',
                                gender=gender,
                                birthday=birthday,
                                )
            try:
                person.full_clean()
            except ValidationError as ex:
                statusmessages.append("Validation errors for %(member_no)s:%(status)s   %(hname)s %(hsir_name)s (%(name)s %(sir_name)s):\n" % row)
                statusmessages.append("    %s\n" % str(ex.message_dict))
                continue
            person.save()
            counts['person'] += 1

            contact = Contact.create_if_nonempty(contact_type_name=ContactType.CT_HOME_PHONE,
                                            person=person,
                                            contact_data=row['home_phone'])
            if (contact != None):
                counts['contact'] += 1
            contact = Contact.create_if_nonempty(contact_type_name=ContactType.CT_MOBILE,
                                           person=person,
                                           contact_data=row['cellular_phone'])
            if (contact != None):
                counts['contact'] += 1
            contact = Contact.create_if_nonempty(contact_type_name=ContactType.CT_FAX,
                                           person=person,
                                           contact_data=row['fax'])
            if (contact != None):
                counts['contact'] += 1

            comment = Comment.create_if_nonempty(person=person, cmt_text=row['comment'])
            if (comment != None):
                counts['comment'] += 1
        # Reset PostgreSQL sequences for the AutoField Persons.id_member.
        if (settings.DATABASES['default']['ENGINE'] == "django.db.backends.postgresql_psycopg2"):
            # We do not want to overload views.py for once off operations.
            from django.db import connection
            import logging
            logger = logging.getLogger(__name__)
            logger.info("Will run raw query PGSQL_RESET_SEQUENCE_SQL.")
            with connection.cursor() as cursor:
                cursor.execute(PGSQL_RESET_SEQUENCE_SQL)
        if (len(statusmessages) > 0):
            return render(request, 'members/longstatus.html', {
                'statusmsg' : "CSV import has been completed: %(person)d persons, %(contact)d contact information records, %(comment)d comment records." % counts,
                'statusmessages' : statusmessages,
                })
        else:
            return HttpResponseRedirect(reverse('main', kwargs={'statusmsg' : "CSV import has been completed: %(person)d persons, %(contact)d contact information records, %(comment)d comment records." % counts}))
    return HttpResponseRedirect(reverse('main', kwargs={'statusmsg' : "CSV import was requested using the wrong method %s, should be POST" % request.method}))


CSV_FIELD_NAMES = ["member_no",
                       "validity",
                       "status",
                       "name",
                       "sir_name",
                       "hname",
                       "hsir_name",
                       "id_no",
                       "home_phone",
                       "cellular_phone",
                       "fax",
                       "email_address",
                       "street_address",
                       "city",
                       "zipcode",
                       "male",
                       "birthdate",
                       "comment",
                       "",     # Append two empty fields at end
                       "",
                       ]

@login_required
def exportCSV(request):
    if request.method == 'GET':
        csvfile = io.StringIO()
        class nlexcel(csv.excel):
            lineterminator = '\n'
        writer = csv.DictWriter(csvfile, dialect=nlexcel(), fieldnames=CSV_FIELD_NAMES)
        writer.writeheader()
        for person in Person.objects.all():
            #if (person.id_member == "31"):
            #    sys.stdout.write(repr(person.csv_row()))
            writer.writerow(person.csv_row())
        csvtext = csvfile.getvalue().encode()
        csvfile.close()
        response = HttpResponse(csvtext, content_type='text/csv')
        response['Content-Disposition'] = 'inline; filename=%s-%s' % (datetime.datetime.now().date(), 'exported.csv')
        return response
    return HttpResponseRedirect(reverse('main', kwargs={'statusmsg' : "CSV export was requested using the wrong method %s, should be GET" % request.method}))

def _create_pdf_table_report(data, top_heading="", report_name='table_report'):
    """Build a PDF file with a table, typically used for creating voters' list for General Assembly.
       Returns HttpResponse.
       report_name - is part of the final report name (which inluces also the creation date).
       field_names - List of fields to be used in the report.
       data - List of Lists, each of them is the data for a single table row.
           The first row should consist of the field names and it gets special treatment.
       top_heading - HTML string to be used as the report's heading.
    """
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="%s-%s.pdf"' % (report_name, datetime.datetime.now().date())
    buffer = io.BytesIO()

    # Create the PDF report
    doc = SimpleDocTemplate(buffer, pagesize=pagesizes.landscape(pagesizes.A4))
    # container for the 'Flowable' objects
    elements = []

    styleSheet = getSampleStyleSheet()
    top_paragraph = [Paragraph(top_heading, styleSheet["BodyText"]), Spacer(0,20)]
    #top_paragraph.drawHeight = 0.5*pagesizes.inch
    #elements.append(top_paragraph)
    #elements.append(Spacer(0,20))

    row_length = len(data[0])
    t=Table([[top_paragraph] + [""]*(row_length-1)] + data,style=[
        ('SPAN',(0,0),(-1,0)),
        ('FONT',(0,0),(-1,0),HEBREW_FONT_NAME,20),
        ('FONT',(0,1),(-1,1),HEBREW_FONT_NAME,16),
        ('FONT',(0,2),(-1,-1),HEBREW_FONT_NAME,12),
        ('BACKGROUND',(0,1),(-1,1),colors.grey),
        ('BOX',(0,1),(-1,-1),2,colors.black),
        ('GRID',(0,1),(-1,-1),0.5,colors.black),
        ],
                repeatRows=2)
    
    elements.append(t)
    # write the document.
    doc.build(elements)
    # Finished creation of the PDF report.
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response

def _visual_order_list(inplist):
    """Process all input tuple/list items, which should be strings, via the BiDi algorithm"""
    return [(get_display(item) if isinstance(item,str) else item) for item in inplist]

@login_required
def report_voters(request):
    field_names = ['id_member',
                       'name_surname_heb', 'name_firstname_heb','name_surname_eng', 'name_firstname_eng',
                       'id_official', 'id_type',
                       'email',
                       'membership_end',
                       'status',  # Serves only for filtering, will be eliminated.
                       ]
    verbose_field_names = ['#'] + [Person._meta.get_field(field_name).verbose_name for field_name in field_names]
    fixed_verbose_field_names = verbose_field_names[0:1] + ["id", "משפחה", "פרטי", "surname", "firstname", "ת.ז."] + verbose_field_names[7:9] + ["membership"]   # instead of too long verbose descriptions

    # We split the computation into persons,voters because
    # filter(voting__exact=True) cannot be used, as voting is
    # a function of status rather than a field.
    persons = Person.objects.order_by('name_surname_heb', 'name_firstname_heb','name_surname_eng', 'name_firstname_eng').values_list(*field_names)
    voters = [person[:-1] for person in persons if Status(person[-1]).can_vote]

    data = [_visual_order_list(fixed_verbose_field_names)] + [[ind+1] + _visual_order_list(voter) for (ind,voter) in enumerate(voters)]

    top_heading ='<para align="center"><font name="%(hebrew_font_name)s" size="20">%(row1)s<br/><br/>%(row2)s<br/></font></para>' % {
        "hebrew_font_name" : HEBREW_FONT_NAME,
        "row1" : get_display('המקור - עמותה ישראלית לתוכנה חופשית ולקוד-מקור פתוח (ע"ר)', base_dir='R'),
        "row2" : get_display("%s %d %s %s" % (
            "רשימת",
            len(voters),
            "בעלי זכות הצבעה נכון לתאריך",
            datetime.datetime.now().date().strftime("%b %d, %Y"))),
        }
    response = _create_pdf_table_report(data,
           top_heading=top_heading,
           report_name='hamakor_voters_report')
    return response


@login_required
def create_pdf_report(request, builder, reportname='pdfreportname'):
    """builder is the function which actually creates the PDF report.
       It is invoked as builder(request, canvas), where canvas is the canvas argument,
       and is expected to call the save() method on the canvas when finished.

       The actual report name incorporates also report creation date.
    """
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="%s-%s.pdf"' % (reportname, datetime.datetime.now().date())
    buffer = io.BytesIO()
    report = canvas.Canvas(buffer)
    builder(request, report)
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response

def pdf_report_pay_reminders(request, canvas):
    canvas.drawString(100, 100, "Report of overdue membership fees is not implemented yet")
    canvas.showPage()
    canvas.save()
    return canvas



@login_required
def report_auditlog(request):
    return HttpResponseRedirect(reverse('main', kwargs={'statusmsg' : "Audit log report is not implemented yet"}))


@login_required
def member_list(request):
    return HttpResponseRedirect(reverse('main', kwargs={'statusmsg' : "Member list access is not implemented yet"}))


@login_required
def member_details(request):
    return HttpResponseRedirect(reverse('main', kwargs={'statusmsg' : "Member details access/edit is not implemented yet"}))

# End of views.py
