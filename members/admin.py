from django.contrib import admin
from django.db import models
from django.forms.widgets import Textarea
from .models import Person, ContactType, Contact, Comment

# Register your models here.

class ShortTextarea(Textarea):
    def __init__(self):
        super().__init__(attrs = {'rows' : '2'})

class ContactInline(admin.TabularInline):
    model = Contact
    extra = 2
class CommentInline(admin.TabularInline):
    model = Comment
    extra = 4
    formfield_overrides = {
        models.TextField : {'widget' : ShortTextarea},
        }

class PersonAdmin(admin.ModelAdmin):
    list_display = [ 'id_member', 'status',
                        'name_surname_heb', 'name_firstname_heb','name_surname_eng', 'name_firstname_eng',
                        'email', 'all_fields_clean'
                        ]
    readonly_fields=('id_member','all_fields_clean','clean_fields_advisory')
    list_display_links = [ 'status',
                               'name_surname_heb', 'name_firstname_heb','name_surname_eng', 'name_firstname_eng',
                               'email',
                               ]
    fieldsets = [
        ('Mandatory', {'fields': ['id_member', 'id_official', 'id_type', 'email','status','membership_end',
                                      'clean_fields_advisory']}),
        ('Name', {'fields': ['name_surname_heb', 'name_firstname_heb','name_surname_eng', 'name_firstname_eng']}),
        ('Mandatory for voting members, optional for non-voters', {'fields': ['membership_start']}),
        ('Address', {'fields': ['address_street_number', 'address_city', 'address_zipcode', 'address_country']}),
        ('Optional for everyone', {'fields': ['gender', 'birthday']}),
        ]
    inlines = [ContactInline, CommentInline]
    list_filter = ['status',]
    search_fields = ['=id_member',
                        '^name_surname_heb', '^name_firstname_heb', '^name_surname_eng', '^name_firstname_eng',
                        '^email',
                        ]


class ContactTypeAdmin(admin.ModelAdmin):
    pass

#class ContactAdmin(admin.ModelAdmin):
#    pass

#class CommentAdmin(admin.ModelAdmin):
#    pass

for (model, model_admin) in [
        (Person, PersonAdmin),
        (ContactType, ContactTypeAdmin),
        #(Contact, ContactAdmin),
        #(Comment, CommentAdmin),
        ]:
    admin.site.register(model, model_admin)
