import enum

from django.core.exceptions import ValidationError
from django.db import models
from django.core.validators import RegexValidator
from django.utils.html import format_html


########################################################################
# Auxiliary functions
########################################################################


class Status(enum.IntEnum):
    ACTIVEMEMBER = 1
    FORMERMEMBER_FRIEND = 2
    FORMERMEMBER_NONFRIEND = 3
    FRIEND = 4

    @property
    def can_vote(self):
        return self == Status.ACTIVE_MEMBER

    @property
    def retain_data(self):
        return self != Status.FRIEND

    @classmethod
    def choices(cls):
      return [
            (cls.ACTIVEMEMBER,           "Member"),
            (cls.FORMERMEMBER_FRIEND,    "Friend/former member"),
            (cls.FORMERMEMBER_NONFRIEND, "Former member"),
            (cls.FRIEND,                 "Friend"),
      ]

########################################################################
####                             Models                             ####
########################################################################

########################################################################
# Person
########################################################################



class Person(models.Model):
    """Members and Friends"""
    IDTYPE_TEUDATZEHUT = 'T'
    IDTYPE_PASSPORT    = 'P'
    IDTYPE_CHOICES = [
        (IDTYPE_TEUDATZEHUT, "Teudat zehut"),
        (IDTYPE_PASSPORT,    "Passport"),
        ]
    id_member = models.AutoField("member id", primary_key=True)
    id_official = models.CharField("government id", max_length=12,
        validators=[RegexValidator(regex=r"^\d+$", message="Only digits in ID number")])
    id_type = models.CharField("id type", max_length=2,
        choices=IDTYPE_CHOICES, default=IDTYPE_TEUDATZEHUT)
    email = models.EmailField("official email")  # TODO: non-empty E-mail address should be unique.
    name_surname_heb = models.CharField("last name hebrew", max_length=64, blank=True)
    name_firstname_heb = models.CharField("first+middle name hebrew", max_length=64, blank=True)
    name_surname_eng = models.CharField("last name english", max_length=64, blank=True)
    name_firstname_eng = models.CharField("first+middle name english", max_length=64, blank=True)
    status = models.IntegerField("status", choices=Status.choices(), default=Status.FRIEND)
    membership_end = models.DateField("membership paid until (inclusive)", null=True, blank=True)
    # The following fields are mandatory for members (self.retain_data()=True), optional for friends (self.retain_data()=False)
    membership_start = models.DateField("membership started date", null=True, blank=True)
    address_street_number = models.CharField("street + home number", max_length=32, blank=True)
    address_city = models.CharField("city", max_length=32, blank=True)
    address_zipcode = models.CharField("zip code", max_length=9, blank=True)
    address_country = models.CharField("country", max_length=3, default="IL", blank=True) # Upgrade to use https://github.com/SmileyChris/django-countries/
    # The following fields are optional for everyone
    GENDER_MALE = "M"
    GENDER_FEMALE = "F"
    GENDER_OTHER = "O" # To be split further if needed
    GENDER_CHOICES = [
        (GENDER_MALE,    "Male"),
        (GENDER_FEMALE,  "Female"),
        (GENDER_OTHER,   "Other"),
        ]
    gender = models.CharField("gender", max_length=2, choices=GENDER_CHOICES, default=GENDER_MALE, null=True, blank=True)
    birthday = models.DateField("birthday", null=True, blank=True)

    def __str__(self):
        return "%s. %s: %s %s (%s %s) %s" % (self.get_status_display(),
            self.id_member,
            self.name_firstname_heb,
            self.name_surname_heb,
            self.name_firstname_eng,
            self.name_surname_eng,
            self.email)

    def _test_nonexcluded_field_emptiness(self, field_name, exclude, errors, verbose_name=None):
        """Test a field in a Person instance for existence of a non-whitespace character.
           field_value - field to be tested. Its name is field.name.
           exclude - list of excluded fields, skip checking if the field is in the list.
           errors - a Dict into which to store the error message, if any.
           verbose_name - for use in the error message. If None, use field.name.
        """
        if field_name in exclude:
            return
        value = self.__dict__[field_name]
        if ((value is None) or (len(value.strip()) == 0)):
            vname = verbose_name if (verbose_name is not None) else field_name
            errors[field_name] = "Empty %s" % vname


    def clean_fields(self, exclude=[]):
        if exclude is None:
            exclude = []
        errors = {}

        if 'id_member' not in exclude:
            if (int(self.id_member) <= 0):
                errors['id_member'] = 'Illegal member ID'

        if self.retain_data():
            # TODO make unconditional after data cleanup.
            self._test_nonexcluded_field_emptiness('id_official', exclude, errors, 'official ID number')

        if 'id_type' not in exclude:
            if (self.id_type not in [choice[0] for choice in Person.IDTYPE_CHOICES]):
                errors['id_type'] = 'Bad official ID type'

        if self.retain_data():
            # TODO make unconditional after data cleanup.
            self._test_nonexcluded_field_emptiness('email', exclude, errors)

        # The rule is that the member/friend must have full name in either Hebrew or English.
        errors2_heb = {}
        self._test_nonexcluded_field_emptiness('name_surname_heb', exclude, errors2_heb, 'Hebrew surname')
        self._test_nonexcluded_field_emptiness('name_firstname_heb', exclude, errors2_heb, 'Hebrew first name')
        errors2_eng = {}
        self._test_nonexcluded_field_emptiness('name_surname_eng', exclude, errors2_eng, 'English surname')
        self._test_nonexcluded_field_emptiness('name_firstname_eng', exclude, errors2_eng, 'English first name')
        if ((len(errors2_heb) > 0) and (len(errors2_eng) > 0)):
            errors.update(errors2_heb)
            errors.update(errors2_eng)

        if self.retain_data():
            # The following fields are optional for yedidim, mandatory for chaverim.
            #if (self.membership_end is None):
            #    # TODO Uncomment after data cleanup
            #    errors['membership_end'] = "Missing membership end date"
            #if (self.membership_start is None):
            #    # TODO Uncomment after data cleanup
            #    errors['membership_start'] = "Missing membership start date"
            self._test_nonexcluded_field_emptiness('address_street_number', exclude, errors, 'street and home number')
            self._test_nonexcluded_field_emptiness('address_city', exclude, errors, 'city name')
            self._test_nonexcluded_field_emptiness('address_zipcode', exclude, errors)
            self._test_nonexcluded_field_emptiness('address_country', exclude, errors, 'country name')
        if errors:
            raise ValidationError(errors)

    def clean_fields_advisory(self):
        """Fields which do not have to be clean, but we want to flag them as dirty.
           The field is read-only.
        """
        errors = []
        if (self.membership_end is None):
            errors.append("Missing membership end date")
        if (self.membership_start is None):
            errors.append("Missing membership start date")
        return format_html("<span style='color: red;'>" + "<br />".join(errors) + "</span>")
    clean_fields_advisory.short_description = ""

    def all_fields_clean(self):
        return not ((self.membership_end is None) or (self.membership_start is None))
    all_fields_clean.boolean = True
    all_fields_clean.short_description = "Data OK?"

    def can_vote(self):
        return Status(self.status).can_vote

    def retain_data(self):
        # Determine whether extra data about the person is mandatory (for members) or optional (for friends)
        return Status(self.status).retain_data

    def _single_contact_of_type(self, contact_type_name):
        """Retrieve contact information of he given type.
           Throw an exception if there are more than one contacts of the given type, as
           the extra ones cannot be exported to the old format CSV file.
        """
        contact_type = ContactType.get_CT(contact_type_name)
        contacts_data = self.contact_set.filter(contact_type=contact_type).values_list('contact_data', flat=True)
        if (len(contacts_data) == 0):
            return None
        elif (len(contacts_data) > 1):
            raise Person.CsvExportException("CSV can use only one instance of this contact type", self.id_member, contact_type, contacts_data)
        return contacts_data[0]

    def csv_row(self):
        """Export the data in self as a Dict suitable for writing out as a CSV record
           in the old CSV format.
        """
        if (self.id_type != Person.IDTYPE_TEUDATZEHUT):
            raise Person.CsvExportException("ID type is not represented in CSV", self.id_member, self.id_type)
        if (self.address_country != 'IL'):
            raise Person.CsvExportException("Non-Israeli member is not represented in CSV", self.id_member, self.address_country)
        if (self.gender == Person.GENDER_MALE):
            male = 't'
        elif (self.gender == Person.GENDER_FEMALE):
            male = 'f'
        elif (self.gender == None):
            male = None
        else:
            raise Person.CsvExportException("Other genders are not represented in CSV", self.id_member, self.gender)

        return {"member_no" : self.id_member,
                    "validity" : "%04d-%02d-%02d" % (self.membership_end.year, self.membership_end.month, self.membership_end.day) if self.membership_end else None,
                    "status" : self.get_status_display(),
                    "name" : self.name_firstname_eng,
                    "sir_name" : self.name_surname_eng,
                    "hname" : self.name_firstname_heb,
                    "hsir_name" : self.name_surname_heb,
                    "id_no" : self.id_official,
                    "home_phone" : self._single_contact_of_type(ContactType.CT_HOME_PHONE),
                    "cellular_phone" : self._single_contact_of_type(ContactType.CT_MOBILE),
                    "fax" : self._single_contact_of_type(ContactType.CT_FAX),
                    "email_address" : self.email,
                    "street_address" : self.address_street_number,
                    "city" : self.address_city,
                    "zipcode" : self.address_zipcode,
                    "male" : male,
                    "birthdate" : "%02d/%02d/%02d" % (self.birthday.day, self.birthday.month, self.birthday.year) if self.birthday != None else None,
                    "comment" : "\n".join(self.comment_set.values_list('cmt_text', flat=True)),
                    "" : "",       # Append an empty field at end
                    }

    ##### Person.Meta ##################################################

    class Meta:
        ordering = ['id_member']
        indexes = [
            models.Index(fields=['id_member']),
            models.Index(fields=['email']),
            models.Index(fields=['name_surname_heb', 'name_firstname_heb', 'name_surname_eng', 'name_firstname_eng']),
            models.Index(fields=['membership_end']),
            ]

    ##### Person.CsvExportException ####################################

    class CsvExportException(Exception):
        pass


########################################################################
# ContactType
########################################################################

class ContactType(models.Model):
    ct_name = models.CharField("contact type", max_length=32, unique=True)
    # Contact types to be used for initial CSV import.
    CT_HOME_PHONE = "home phone"
    CT_MOBILE = "mobile"
    CT_FAX = "FAX"
    CT_EMAIL = "email"

    # Array of contact type rows for use by CSV import.
    CT = {}

    @classmethod
    def get_CT(cls, ct_name):
        if ct_name not in ContactType.CT:
            cts = ContactType.objects.filter(ct_name=ct_name)
            if (len(cts) > 0):
                if (len(cts) > 1):
                    import logging
                    logger = logging.getLogger(__name__)
                    logger.error("More than one ContactType record with name=%s", ct_name)
                ct = cts[0]
            else:
                ct = ContactType(ct_name=ct_name)
                ct.save()
            ContactType.CT[ct_name] = ct
        return ContactType.CT[ct_name]

    def __str__(self):
        return self.ct_name

    ##### ContactType.Meta #################################################
    class Meta:
        indexes = [
            models.Index(fields=['ct_name']),
            ]

########################################################################
# Contact
########################################################################

class Contact(models.Model):
    """Contact information item"""
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    contact_type = models.ForeignKey(ContactType, on_delete=models.CASCADE)
    contact_data = models.CharField("contact data", max_length=64)
    use_voice = models.BooleanField("use voice", default=True)
    use_sms = models.BooleanField("use SMS", default=True)

    def __str__(self):
        return "%s %s: %s = %s" % (self.person.name_firstname_heb,
            self.person.name_surname_heb,
            self.contact_type,
            self.contact_data)

    DEFAULT_USES = {       # Relevant only for CSV import.
        ContactType.CT_HOME_PHONE : { "use_voice" : True,  "use_sms" : False  },
        ContactType.CT_MOBILE :     { "use_voice" : True,  "use_sms" : True   },
        ContactType.CT_FAX :        { "use_voice" : False, "use_sms" : False  },
        ContactType.CT_EMAIL :      { "use_voice" : False, "use_sms" : False  },
        }

    @classmethod
    def create_if_nonempty(cls, contact_type_name, person, contact_data):
        if ((contact_data != None) and (len(contact_data.strip()) > 0)):
            contact = cls(person=person,
                              contact_type=ContactType.get_CT(contact_type_name),
                              contact_data=contact_data.strip(),
                              **cls.DEFAULT_USES[contact_type_name])
            contact.save()
            return contact
        else:
            return None

    ##### Contact.Meta #################################################
    class Meta:
        ordering = ['person', 'contact_type']
        indexes = [
            models.Index(fields=['person', 'contact_type']),
            ]


########################################################################
# Comment
########################################################################

class Comment(models.Model):
    """Per-person comment record"""
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    cmt_date = models.DateTimeField("comment was added at", auto_now_add=True)
    cmt_text = models.TextField("comment")

    def __str__(self):
        return "%s %s: %s: %s" % (self.person.name_firstname_heb,
                                      self.person.name_surname_heb,
                                      str(self.cmt_date),
                                      self.cmt_text)
    @classmethod
    def create_if_nonempty(cls, person, cmt_text):
        if ((cmt_text != None) and (len(cmt_text.strip()) > 0)):
            comment = cls(person=person,
                              cmt_text=cmt_text.strip())
            comment.save()
            return comment
        else:
            return None

    ##### Comment.Meta #################################################

    class Meta:
        ordering = ['person', 'cmt_date']
        indexes = [
            models.Index(fields=['person', 'cmt_date']),
            ]



# End of models.py
