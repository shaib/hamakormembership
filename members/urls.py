from django.urls import path

from . import views

urlpatterns = [
    path('db_csv/import', views.importCSV, name='db_csv_import'),
    path('db_csv/export', views.exportCSV, name='db_csv_export'),
    path('report/voters', views.report_voters, name='report_voters'),
    path('report/pay_reminders', lambda request: views.create_pdf_report(request, views.pdf_report_pay_reminders), name='report_pay_reminders'),
    path('report/auditlog', views.report_auditlog, name='report_auditlog'),
    path('members/list', views.member_list, name='members_list'),
    path('member/details/<int:id_member>', views.member_details, name='member_details'),
]
